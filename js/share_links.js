/**
 * @file
 * JS for Oxbow Share Module
 *
 */
 
(function ($) { // jQuery <3 Drupal
  $(document).ready(function() {
  
  
  
    var $window = $(window);
    var $share = $('.share-links.sticky-able');
    var $share_parent = null;
    var share_top = 0;
    
    if ($share.length) {
      
      // get nearest relative/fixed/static parent
      $tmp_parent = $share.parent();
      while($share_parent == null) {
        if ($tmp_parent.css('position') != 'static' || $tmp_parent.attr('tagName') == 'BODY') {
          $share_parent = $tmp_parent;
        }
        else {
          $tmp_parent = $tmp_parent.parent();
        }
      }
      
      share_top = $share.offset().top;
      share_left = 0 - $share_parent.offset().left;
    
    
      if ($window.height() < $share.height()+share_top) {
        $share.addClass('no-sticky');
        $share.css('left', share_left+'px');
      }
    }
  
  }); // End Doc Ready
})(jQuery); // End jQuery <3 Drupal