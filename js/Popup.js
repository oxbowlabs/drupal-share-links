/**
 * @file
 * JS for Oxbow Share Module
 *
 */
 
(function ($) { // jQuery <3 Drupal  

 window.Popup = function(_body, _title) {
  
  // popup target
  var popup = $('.popup');
  
  // create popup if it does not exist
  if (!popup.length) {
   $('body').prepend('<div class="popup"></div>');
   popup = $('.popup');
  }
  
  // clear popup and add base containers
  popup.html('');
  popup.append('<div class="background"></div>');
  popup.append('<div class="canvas"></div>');
  var background = popup.find('.background');
  var canvas = popup.find('.canvas');
  
  // add page container
  canvas.append('<div class="page"></div>');
  var page = popup.find('.page');
  
  // add menu
  var menu_content = '<a class="close_btn"><</a><h2 class="popup_title">'+_title+'</h2><a class="send_btn">Send</a>';
  page.append('<div class="popup_header">' + menu_content + '</div>');
  
  // add body
  page.append('<div class="popup_body"></div>');
  var body = popup.find('.popup_body');
  var body_content = $(_body);
  var body_parent = '';
  var body_display = '';
  
  // add popup messages
  body.append('<p class="popup_msg"></p>');
  
  // if body is an element, APPEND TO popup body   
  if (body_content.length) {
   var body_target = body_content;
   body_parent = body_content.parent();
   body_target.appendTo(body);
   body_display = body_target.css('display');
   body_target.css('display', 'block');
  }
  // if body is text, insert into popup body
  else {
   body_content = '';
   body_content += '<p>' + _body + '</p>';
   body.append(body_content);
  }
  
  
  
  body.click(function(e) {
    e.stopPropagation();
  });
  popup.find('.popup_header').click(function(e) {
    e.stopPropagation();
  });
  
  /// Popup Close
  canvas.add('.close_btn').click(function( event ) {
   // prevent clicking behind popup
   event.stopPropagation();
   // prevent close if sending
   if (!$('.popup').hasClass('sending').length) {
    // move body element back to original location
    if (body_parent != '') {
     body_target.css('display', body_display);
     body_target.appendTo(body_parent);
    }
    // remove and clear popup
    popup.empty()
    popup.remove();
   }
   
  });
  
  
  
  /// Form Submit
  popup.find('.send_btn').click(function(e) {
    popup.find('.popup_form').submit();
  });
   
   
  popup.find('.popup_form').submit(function(e) {
    
   var target = $('.popup');
   
   // prevent send if already sending
   if (target.hasClass('sending')) {
    e.preventDefault();
   }
   
   // if not sending
   else {
    
    var send = true;
    var email_check = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i;
    var msg = target.find('.popup_msg');
    msg.html('');
    msg.removeClass('has-msg');
    
    // check if each input is complete
    target.find('input:not(#edit-url)').each(function() {
     // if not complete prevent send and assign alert message
     if ($(this).attr('value') == '' && send) {
      e.preventDefault();
      msg.html('Please fill out "' + $.trim($(this).siblings('label').html()) + '"');
      msg.addClass('has-msg');
      send = false;
     }
     // if an email address check if valid
     if ($(this).hasClass('form-email') && !email_check.test($(this).attr('value')) && send) {
      e.preventDefault();
      msg.html('Please enter a valid email for "' + $.trim($(this).siblings('label').html()) + '"');
      msg.addClass('has-msg');
      send = false;
     }
    });
    if (send) {
     target.addClass('sending');
     msg.html('Sending, please wait.');
     msg.addClass('has-msg');
    }
    else {
      target.find('.popup_body').scrollTo(0);
    }
   }
  });
  
 }
  
})(jQuery); // End jQuery <3 Drupal