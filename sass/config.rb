# Require any additional compass plugins here.
require 'zen-grids'
require 'compass-normalize'
require 'breakpoint'

http_path = "/sites/all/themes/thewheel/sass"
css_dir = "../css"
sass_dir = "."
images_dir = "../images/sprites"
javascripts_dir = "../js"
relative_assets = true
line_comments = true